const fetch = require('node-fetch')
const _ = require('lodash');
const express = require('express')
const app = express()
const port = 3000

let url = "http://www.json-generator.com/api/json/get/bTvDGAmlnS?indent=2";

let settings = {method: "Get"};


//get the connected user
//TODO: add the code to get the connected user from firebase
function getConnectedUser(){
 return {
     role: ["school-admn", "teacher"]
 };
}

//get the permissions settings files for pages
//TODO: get properly the permissions for pages in the database
function getPageRuleSettings(){
 return {
      "Page_Id":{
        "name": "First Page",
        "url": "htttps://family.arabeelearning.com",
        "roles": ["Admin","Teacher"],
        "children": {
          "Component-Id": {
            "name": "first Component",
            "roles": ["Admin", "Teacher"],
            "children": {
              "Component-Id":{
                "name": "childComponent",
                  "roles":["Teacher", "Admin"]
              }
            }
           },
          "Component-Id2":{
            "name": "second Component",
            "roles": ["Teacher"],
            "children": {}
           }
        }
      },
      "Page_Id2":{
        "name": "First Page",
        "url": "htttps://family.arabeelearning.com",
        "roles": ["Admin","Teacher"],
        "children": {
          "Component-Id": {
            "name": "first Component",
            "roles": ["Student"],
            "children": {}
           },
          "Component-Id2":{
            "name": "second Component",
            "roles": ["Teacher"],
            "children": {}
           }
        }
      }
    };
  
  
}


//Parse a node of page rule
/**
 *  this function takes a node or item from the page rule settings and returns 
 *  a json of the available component in the node based on the user role
 * example of node
 * {
    "name": "first Component",
    "roles": ["Admin", "Teacher"],
    "children": {
        "Component-Id":{
        "name": "childComponent",
            "roles":["Teacher", "Admin"]
        }
    }
 * 
 * 
 * Examples of values to return:
 * 
 * sample 1
 * {
 *  "result": true
 * }
 * 
 * sample 2
 * * {
 *  "result": true,
 *  "children": {
 *     "elementID": {
 *        "result": true
 *      }
 *   }
 * }
 * 
 * */
function parsePageRuleNode(node, userRoles){
    let resp = {"result": false};
    userRoles.map(role =>{
        if(node.roles && node.roles.contains(role)){
            resp.result = true;
            if(node.children){
                if(!resp.children){
                    resp.children = {};
                }
                Object.keys(node.children).map(subNodeKey => {
                 let subNode = node.children[subNodeKey];
                 resp.children[subNodeKey] = parsePageRuleNode(subNode, userRoles);
                });            
            }
        }      
    })
    return resp;
}


//getPageRulePermissions
/*
* this function parse all the setting from all page rules settings and returns
*  a json of the available components for all those pages  based on the user role
*/
function getPageRulePermissions(userRoles){
    let resp = {};
    let pageRulesSettings = getPageRuleSettings();
    Object.keys(pageRulesSettings).map(nodeKey => {
        let node = pageRulesSettings[nodeKey];
        resp[nodeKey] = parsePageRuleNode(node);
    })
    return resp;
}

app.get('/', (req, res) => getPageRulePermissions(getConnectedUser()))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))